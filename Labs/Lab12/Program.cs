﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    class Program
    {
        static int[] myIntegers = { 10, 4, 20, 5, 30, 6 };
        static int[] myIntegersCopy;
        static string[] myStrings = { "Hello", "Aisha", "Hope", "To", "See", "You", "Again" };
        static string[] myStringsCopy;

        static void Main(string[] args)
        {

            myIntegersCopy = new int[myIntegers.Length];
            Array.Copy(myIntegers, myIntegersCopy, myIntegers.Length);

            Console.Write("\nInitial Integer Array: ");
            PrintArray(myIntegers);

            Console.Write("\nCopied Integer Array: ");
            PrintArray(myIntegersCopy);

            Array.Sort(myIntegers);
            Console.Write("\nSorted Integer Array: ");
            PrintArray(myIntegers);

            Array.Reverse(myIntegers);
            Console.Write("\nReversed Integer Array: ");
            PrintArray(myIntegers);

            Array.Clear(myIntegers,0,myIntegers.Length);
            Console.Write("\nInteger Array after clear method: ");
            PrintArray(myIntegers);

            Console.WriteLine();

            myStringsCopy = new string[myStrings.Length];
            Array.Copy(myStrings, myStringsCopy, myStrings.Length);

            Console.Write("\nInitial String Array: ");
            PrintArray(myStrings);

            Console.Write("\nCopied String Array: ");
            PrintArray(myStringsCopy);

            Array.Sort(myStrings);
            Console.Write("\nSorted String Array: ");
            PrintArray(myStrings);

            Array.Reverse(myStrings);
            Console.Write("\nReversed String Array: ");
            PrintArray(myStrings);

            Array.Clear(myStrings, 0, myStrings.Length);
            Console.Write("\nString Array after clear method: ");
            PrintArray(myStrings);

            Console.ReadLine();

        }

        private static void PrintArray(string[] myStrings)
        {
            foreach (var str in myStrings)
            {
                Console.Write(str + " ");
            }
        }

        private static void PrintArray(int[] myIntegers)
        {
            foreach (var a in myIntegers)
            {
                Console.Write(a + " ");
            }
        }
    }
}
