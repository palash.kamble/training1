﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    public class Employee
    {
        private int EmpID;
        private string EmpName;
        private string EmpDesignation;
        private double BasicSal;
        private double HRA;
        private double DA;
        private double PF;
        private double GrossSalary;
        private double NetSalary;
        private int DeptID;

        static int count = 0;
        static int autoID = 1000;

        private static int generateID()
        {
            autoID += 1;
            return autoID;
        }

        public int EID { get { return EmpID; } set { EmpID = autoID++; } }
        public string EName { get { return EmpName; } set { EmpName = value; } }
        public string EDesignation { get { return EmpDesignation; } set { EmpDesignation = value; } }
        public int EDeptID { get { return DeptID; } set { DeptID = value; } }
        public double EBasicSal { get { return BasicSal; } set { BasicSal = value; } }
        public double EDA { get { return DA; } set { DA = value; } }
        public double EHRA { get { return HRA; } set { HRA = value; } }
        public double EPF { get { return PF; } set { PF = value; } }
        public double EGrossSalary { get { return GrossSalary; } set { GrossSalary = value; } }
        public double ENetSalary { get { return NetSalary; } set { NetSalary = value; } }

        public static int getCount()
        {
            return count;
        }

        public override string ToString()
        {
            return string.Format("Employee ID: {0}\tEmployee name: {1}", EID, EName);
        }
    }
}
