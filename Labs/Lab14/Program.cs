﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Program
    {
        static LinkedList<Employee> employees;
        static int autoID = 1000;
        static void Main(string[] args)
        {
            employees = new LinkedList<Employee>();
            bool ch = true;
            while (ch)
            {

                Console.WriteLine("Press 1 to add employee details\nPress 2 to get total number of employees" +
                    "\nPress 3 to display list of all employees\n");
                int ch1 = Convert.ToInt32(Console.ReadLine());
                switch (ch1)
                {
                    case 1:
                        Console.WriteLine("Enter Employee Name:");
                        string name = Console.ReadLine();
                        Console.WriteLine("Enter Employee Designation:");
                        string desig = Console.ReadLine();
                        Console.WriteLine("Enter Employee Basic Salary:");
                        double sal = Convert.ToDouble(Console.ReadLine());
                        employees.AddLast(new Employee {EID = autoGenerateID(), EName = name,EDesignation = desig, EBasicSal = sal });
                        break;
                    case 2:
                        Console.WriteLine("Total number of Employees: " + employees.Count);
                        break;
                    case 3:
                        PrintList();
                        break;
                    default:
                        break;
                }
                
            }
            Console.ReadLine();
        }

        private static int autoGenerateID()
        {
            autoID += 1;
            return autoID;
        }

        private static void PrintList()
        {
            foreach (var item in employees)
            {
                Console.WriteLine(item);
            }
        }
    }
}
