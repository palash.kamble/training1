﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab13
{
    public class Department
    {
        private int DeptID;
        private string Deptname;
        private string DeptLocation;

        public int DID { get; set; }
        public string DName { get; set; }
        public string DLocation { get; set; }

        public override string ToString()
        {
            return string.Format("Department ID: {0}\tDepartment Name: {1}\tDepartment Location: {2}", DID, DName, DLocation);
        }

    }
}
