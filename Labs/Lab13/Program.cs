﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Lab13
{
    class Program
    {
        static ArrayList alist;
        static void Main(string[] args)
        {

            alist = new ArrayList();
            bool ct = true;
            while (ct)
            {
                Console.WriteLine("Enter Department ID: ");
                int did = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Department Name: ");
                string dname = Console.ReadLine();
                Console.WriteLine("Enter Department Location: ");
                string dlocation = Console.ReadLine();

                alist.Add(new Department { DID = did, DName = dname, DLocation = dlocation });
                Console.WriteLine("Press 1 to add more 0 to display all departments");
                int ch = Convert.ToInt32(Console.ReadLine());
                if (ch!=1)
                {
                    ct = false;
                }
            }

            PrintArrayList();

            Console.ReadLine();

        }

        private static void PrintArrayList()
        {
            foreach (var item in alist)
            {
                Console.WriteLine(item);
            }
        }
    }
}
