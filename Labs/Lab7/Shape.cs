﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public abstract class Shape
    {
        public abstract void Area();
        public abstract void displayArea();
    }

    public class Circle : Shape
    {
        private int radius;
        private double area;
        public Circle(int _radius)
        {
            radius = _radius;
        }
        public override void Area()
        {
            area = Math.PI * radius * radius;
        }
        public override void displayArea()
        {
            Console.WriteLine("Area of circle:" + area);
        }
    }

    public class Rectangle : Shape
    {
        private int length;
        private int breadth;
        private double area;
        public Rectangle(int _length,int _breadth)
        {
            length = _length;
            breadth = _breadth;
        }
        public override void Area()
        {
            area = length * breadth;
        }
        public override void displayArea()
        {
            Console.WriteLine("Area of rectangle:" + area);
        }
    }
}
