﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide Employee Details");
            Console.WriteLine("Employee ID:");
            int eid = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Employee Name:");
            string ename = Console.ReadLine();

            Console.WriteLine("Employee Designation:");
            string edesig = Console.ReadLine();

            Console.WriteLine("Employee Basic Salary");
            double ebasicSal = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter DA:");
            double eDA = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter Department ID:");
            int eDeptID = Convert.ToInt32(Console.ReadLine());
            


            Employee e1 = new Employee(ebasicSal,eDA);
            e1.EmployeeID = eid;
            e1.EmployeeName = ename;
            e1.EmployeeDesignation = edesig;
            e1.EmployeeBasicSalary = ebasicSal;
            e1.EmployeeDA = eDA;
            e1.EmpDeptID = eDeptID;

            Department d1 = new Department();

            Console.WriteLine("Provide Department Details");

            Console.WriteLine("Enter Department ID");
            d1.DepartmentID = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Department Name");
            d1.DepartmentName = Console.ReadLine();

            Console.WriteLine("Enter Department Location");
            d1.DepartmentLocation = Console.ReadLine();

            Console.WriteLine("Press e to show employee details");
            Console.WriteLine("Press d to show department details");
            string ch = Console.ReadLine();

            switch (ch)
            {
                case "e":
                    Console.WriteLine("Employee ID:" + e1.EmployeeID);
                    Console.WriteLine("Employee Name:" + e1.EmployeeName);
                    Console.WriteLine("Employee Designation:" + e1.EmployeeDesignation);
                    Console.WriteLine("Employee Basic Salary:" + "Rs. " + e1.EmployeeBasicSalary);
                    Console.WriteLine("Employee HRA:" + e1.EmployeeHRA);
                    Console.WriteLine("Employee DA:" + e1.EmployeeDA);
                    Console.WriteLine("Employee PF:" + e1.EmployeePF);
                    Console.WriteLine("Employee Gross Salary:" + e1.EmployeeGrossSalary);
                    Console.WriteLine("Employee Net Salary:" + e1.EmployeeNetSalary);
                    break;
                case "d":
                    Console.WriteLine("Department ID:" + d1.DepartmentID);
                    Console.WriteLine("Department Name:" + d1.DepartmentName);
                    Console.WriteLine("Department Location:" + d1.DepartmentLocation);
                    break;
                default:
                    break;

            }

        }

        
    }
}
