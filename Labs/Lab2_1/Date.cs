﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_1
{
    public class Date
    {
        public int mDate { get; set; }
        public int mMonth { get; set; }
        public int mYear { get; set; }

        public Date() 
        {
            Console.WriteLine("/////////////// In the default constructor");
            Console.WriteLine("Enter date");
            mDate = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter month");
            mMonth = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter year");
            mYear = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(mDate + "-" + mMonth + "-" + mYear);

        }

        public Date(int _date,int _month,int _year)
        {
            mDate = _date;
            mMonth = _month;
            mYear = _year;
        } 

    }
}
