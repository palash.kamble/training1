﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    public class Employee
    {
        private int EmpID;
        private string EmpName;
        private string EmpDesignation;
        private double BasicSal;
        private double HRA;
        private double DA;
        private double PF;
        private double GrossSalary;
        private double NetSalary;
        private int DeptID;


        // static variable
        private static int Count = 0;
            
        private static int autoID = 10000;

        // Default Constructor
        public Employee()
        {
            Count++;
            autoID++;
        }

        // static method to access count of employees
        public static int getCount()
        {
            return Count;
        }

        public int EID { get { return EmpID; } set { EmpID = value; } }
        public string EName { get {return EmpName ; } set {EmpName = value; } }
        public string EDesignation { get { return EmpDesignation; } set { EmpDesignation = value; } }
        public int EDeptID { get { return DeptID; } set { DeptID = value; } }
        public double EBasicSal { get { return BasicSal; } set { BasicSal = value; } }
        public double EDA { get { return DA; } set { DA = value; } }
        public double EHRA { get { return HRA; } set { HRA = value; } }
        public double EPF { get { return PF; } set { PF = value; } }
        public double EGrossSalary { get { return GrossSalary; } set { GrossSalary = value; } }
        public double ENetSalary { get { return NetSalary; } set { NetSalary = value; } }

        public void getDetails()
        {
            EID = autoID;
            Console.WriteLine("\nProvide details of Employee");
            Console.WriteLine("Employee ID:" + EmpID);
            Console.Write("Enter name:");
            EName = Console.ReadLine();
            Console.Write("Enter designation:");
            EDesignation = Console.ReadLine();
            Console.Write("Enter Department ID:");
            EDeptID = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter basic salary:");
            EBasicSal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter dearness allowance:");
            EDA = Convert.ToDouble(Console.ReadLine());

            calculateSalary();
        }

        private void calculateSalary()
        {
            EHRA = (8 * BasicSal) / 100;
            EPF = (12 * BasicSal) / 100;
            EGrossSalary = BasicSal + HRA + 1250;
            ENetSalary = GrossSalary + (10000 - PF);
        }

        // ToString method
        public override string ToString()
        {
            return string.Format("Employee ID: {0}\nEmployee Name: {1}\nEmployee Gross Salary: {2}",EmpID,EmpName,GrossSalary);
        }

    }
}
