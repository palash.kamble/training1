﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_1
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee e1 = new Employee();
            e1.getDetails();
            Employee e2 = new Employee();
            e2.getDetails();
            Console.WriteLine("---------------------");
            Console.WriteLine();
            Console.WriteLine(e1);
            Console.WriteLine("---------------------");
            Console.WriteLine();
            Console.WriteLine(e2);

        }
    }
}
