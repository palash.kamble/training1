﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp
{
    public class Product
    {
        public void SaySomething()
        {
            Console.WriteLine("Product.SaySomething is running on thread number: {0}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
