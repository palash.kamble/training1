﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelismApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Using c# For Loop.........{0}",DateTime.Now.ToString());
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i = {0},thread = {1}", i,Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(100);
            }
            Console.WriteLine("Using c# For Loop.........{0}", DateTime.Now.ToString());
            
            Console.WriteLine("Using c# 4 Parallel.For Loop...........{0}",DateTime.Now.ToString());
            Parallel.For(0, 10, i => {
                Console.WriteLine("i = {0},thread = {1}", i, Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(100);
            });

            Console.WriteLine("Using c# For Loop.........{0}", DateTime.Now.ToString());
            Console.ReadLine();
        }
    }
}
