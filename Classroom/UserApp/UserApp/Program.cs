﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp
{
   
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Enter Username");
            string username = Console.ReadLine();
            Console.WriteLine("Enter Password");
            string password = Console.ReadLine();

            Login login = new Login();
            login.LoginSuccessful += OnSuccess;
            login.LoginFailed += OnFail;


            User u1 = new User { UserName = username, Password = password};
            
            login.LoginResult(u1);
                          

        }

        private static void OnFail(LoginEventArgs l)
        {
            Console.WriteLine("Failed\nWrong Credentials");

            Console.WriteLine("Username: {0}\nPassword: {1}", l.Username, l.Password);
        }

        private static void OnSuccess(LoginEventArgs l)
        {
            Console.WriteLine("Login Successfull\nWelcome to Flight Network");
            Console.WriteLine("Your credentials");
            Console.WriteLine("Username: {0}\nPassword: {1}", l.Username, l.Password);
        }
    }

    
}
