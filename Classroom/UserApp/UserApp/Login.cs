﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp
{
    public delegate void LoginDelegate(LoginEventArgs l);

    public class Login
    {
        public event LoginDelegate LoginSuccessful;
        public event LoginDelegate LoginFailed;

        //public void LoginSuccess(User u)
        //{
        //    LoginSuccessful(u);
        //    LoginFailed(u);
        //}

        public static bool Authenticate(User user)
        { 
            if (user.UserName == "flight" && user.Password == "flight")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void LoginResult(User u1)
        {
            bool check = Authenticate(u1);
            if (check)
            {
                LoginEventArgs l = new LoginEventArgs { Username = u1.UserName, Password = u1.Password };
                LoginSuccessful(l);
            }
            else
            {
                LoginFailed(new LoginEventArgs {Username = u1.UserName,Password = u1.Password });
            }
        }

      
    }

}
