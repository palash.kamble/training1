﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Please enter full path of dll to be loaded: ");
            //string asm = Console.ReadLine();

            // load the assembly of given path
            Assembly assembly = Assembly.LoadFile(@"C:\training1\Classroom\ReflectionApp\TripStackLib\bin\Debug\TripStackLib.dll");

            //// get all the types
            //Type[] types = assembly.GetTypes();

            //// enumerate through this collection
            //foreach (var item in types)
            //{
            //    Console.WriteLine("Type Name: {0}",item.Name);
            //    Console.WriteLine("Is Class?: {0}",item.IsClass);
            //    Console.WriteLine();

            //    if (item.IsClass)
            //    {
            //        foreach (MethodInfo mi in item.GetMethods())
            //        {
            //            Console.WriteLine("Name: {0}",mi.Name);
            //        }
            //    }

            //}

            Console.WriteLine("1. Addtion");
            Console.WriteLine("2. Subtraction");
            Console.WriteLine("3. Square");
            Console.WriteLine("4. Cube");
            Console.WriteLine("Enter operation number: ");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Type t = assembly.GetType("TripStackLib.ComplexMath");
                    // get default constructor
                    ConstructorInfo constructor = t.GetConstructor(Type.EmptyTypes);
                    // construct an object
                    object instance = constructor.Invoke(null);
                    // get method to be invoked
                    MethodInfo method = t.GetMethod("Add");

                    Console.WriteLine("Enter first number: ");
                    int x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number: ");
                    int y = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters = { x, y };

                    // invoke method
                    object result = method.Invoke(instance,parameters);
                    Console.WriteLine("Addition of {0} and {1} is {2}",x,y,result);

                    break;
                case 2:
                    Type t2 = assembly.GetType("TripStackLib.ComplexMath");
                    ConstructorInfo constructor2 = t2.GetConstructor(Type.EmptyTypes);
                    object instance2 = constructor2.Invoke(null);
                    MethodInfo method2 = t2.GetMethod("Sub");
                    Console.WriteLine("Enter first number: ");
                    int x2 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number: ");
                    int y2 = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters2 = { x2, y2 };

                    // invoke method
                    object result2 = method2.Invoke(instance2, parameters2);
                    Console.WriteLine("Subtraction of {0} and {1} is {2}", x2, y2, result2);
                    break;
                case 3:
                    Type t3 = assembly.GetType("TripStackLib.SimpleMath");
                    ConstructorInfo constructor3 = t3.GetConstructor(Type.EmptyTypes);
                    object instance3 = constructor3.Invoke(null);
                    MethodInfo method3 = t3.GetMethod("Square");
                    Console.WriteLine("Enter first number: ");
                    int x3 = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters3 = { x3 };

                    // invoke method
                    object result3 = method3.Invoke(instance3, parameters3);
                    Console.WriteLine("Square of {0}  is {1}", x3, result3);

                    break;
                case 4:
                    Type t4 = assembly.GetType("TripStackLib.SimpleMath");
                    ConstructorInfo constructor4 = t4.GetConstructor(Type.EmptyTypes);
                    object instance4 = constructor4.Invoke(null);
                    MethodInfo method4 = t4.GetMethod("Cube");
                    Console.WriteLine("Enter first number: ");
                    int x4 = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters4 = { x4 };

                    // invoke method
                    object result4 = method4.Invoke(instance4, parameters4);
                    Console.WriteLine("Cube of {0}  is {1}", x4, result4);

                    break;
                default:
                    break;
            }

            Console.ReadLine();

        }
    }
}
