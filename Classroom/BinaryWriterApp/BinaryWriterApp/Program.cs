﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Open or create a file
            //FileStream fs = File.OpenWrite(@"C:\training1\product.txt");

            //BinaryWriter writer = new BinaryWriter(fs);

            //// product id
            //writer.Write(1234);
            //// name
            //writer.Write("Product 1");
            //// price
            //writer.Write(56.99);

            //writer.Close();
            //fs.Close();

            //Console.WriteLine("Product details are written to a file");

            FileStream fs = File.OpenRead(@"C:\training1\product.txt");

            BinaryReader reader = new BinaryReader(fs);

            Console.WriteLine("Product ID: " + reader.ReadInt32());
            Console.WriteLine("Product Name: " + reader.ReadString());
            Console.WriteLine("Product Price: " + reader.ReadDouble());


            reader.Close();
            fs.Close();

            Console.ReadLine();

        }
    }
}
