﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamReadWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // create file with enable appending
            StreamWriter writer = new StreamWriter(@"C:\training1\data.txt", true);
            writer.WriteLine("This is line one");
            writer.WriteLine("This is line two");
            writer.WriteLine("This is line three");
            writer.Close();

            Console.WriteLine("Press a key to read it back:");
            Console.ReadKey();
            Console.WriteLine();
            StreamReader reader = new StreamReader(@"C:\training1\data.txt");
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);

            }
                reader.Close();

                Console.ReadLine();
            
        }
    }
}
