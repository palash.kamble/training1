﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebuggerApp
{
    public class Account
    {
        public decimal Balance
        {
            get
            {
                return this.Balance;
            }
            set
            {
                if (value >= 0)
                {
                    this.Balance = value;
                }
            }
        }

        public Account(decimal intBalance)
        {
            Balance = intBalance;
        }

        public void Credit(decimal amount)
        {
            Balance += amount;
        }
        public void Debit(decimal amount)
        {
            if (amount > Balance)
            {
                Console.WriteLine("Debit amount is exceede balance");
            }
            if (amount <= Balance)
            {
                Balance -= amount;
            }
        }
    }
}
