﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; 

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\Windows");

            Console.WriteLine("Listing contents of {0} directory: ", di.FullName);
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                Console.WriteLine("Folder name: " + dir.Name);
                DirectoryInfo dii = new DirectoryInfo(@"C:\Windows\" + dir.Name);
                foreach (FileInfo file1 in dii.GetFiles())
                {
                    try
                    {
                        Console.WriteLine("File Name: " + file1.Name);
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        
                    }
                    
                }
                Console.WriteLine();
                //foreach (FileInfo file in di.GetFiles())
                //{
                //    Console.WriteLine("File Name: " + file.Name);
                //    Console.WriteLine("File size in (bytes): " + file.Length);
                //    Console.WriteLine("Creation time: " + file.CreationTime);
                //    Console.WriteLine();
                //}

                
            }
        }
    }
}
