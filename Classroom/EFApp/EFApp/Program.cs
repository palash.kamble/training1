﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();

            //foreach (var item in db.Products.Where(p => p.Price > 30))
            //{
            //    Console.WriteLine("ID: {0}\t" +
            //        "Name: {1}\t" +
            //        "Description: {2}\t" +
            //        "Category: {3}\t" +
            //        "Price: {4}",
            //        item.Id,item.Name,item.Description,item.Category,item.Price);
            //}

            // Add
            //Product prd = new Product { Name = "Cricket Ball", Description = "For legend", Category = "cricket", Price = 321 };
            //db.Products.Add(prd);

            // Update
            //var prd = db.Products.Where(p => p.Id == 11).SingleOrDefault();
            //prd.Price += 100;

            // Delete
            var prd = db.Products.Where(p => p.Id == 11).SingleOrDefault();
            db.Products.Remove(prd);

            db.SaveChanges();
            Console.WriteLine("Product added...");
            Console.ReadLine();
        }
    }
}
