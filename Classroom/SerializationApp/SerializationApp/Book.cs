﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    [Serializable]
    public class Book
    {
        public int BookID { get; set; }
        public string BookName { get; set; }
        public double BookPrice { get; set; }

        string publisher;

        public void SetPublisher(string publisher)
        {
            this.publisher = publisher;
        }

        public override string ToString()
        {
            return string.Format("Book ID: {0}\tBook Name: {1}\tBook Price: {2}\tBook Publisher: {3}", BookID, BookName, BookPrice, this.publisher);
        }

    }
}
