﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // create book.txt to store all book information
            FileStream fs = File.OpenWrite(@"C:\training1\books.txt");

            // create formatter for storing books in books.txt
            BinaryFormatter bf = new BinaryFormatter();

            Book b1 = new Book { BookID = 100, BookName = "Intro to C#", BookPrice = 245 };
            b1.SetPublisher("Packet");

            Book b2 = new Book { BookID = 101, BookName = "Pro C#", BookPrice = 345 };
            b2.SetPublisher("APress");

            List<Book> books = new List<Book> { b1, b2 };

            bf.Serialize(fs, books);
            //bf.Serialize(fs, b2);

            

            fs.Close();

            Console.WriteLine("Book stored in books.txt file");

            fs = File.OpenRead(@"C:\training1\books.txt");

            //Book b = (Book)bf.Deserialize(fs);

            List<Book> b = (List<Book>)bf.Deserialize(fs);

            foreach (var book in b)
            {
                Console.WriteLine(book);
            }

            //while (fs.Position != fs.Length)
            //{
            //    Book b = (Book)bf.Deserialize(fs);
            //    Console.WriteLine(b);
            //}


            fs.Close();


            Console.ReadLine();

        }
    }
}
