﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter following values:");
            Console.WriteLine("Enter Name"); ;
            string name = Console.ReadLine();
            Console.WriteLine("Enter Description");
            string desc = Console.ReadLine();
            Console.WriteLine("Enter Category");
            string cat = Console.ReadLine();
            Console.WriteLine("Enter Price");
            double price = Convert.ToDouble(Console.ReadLine());

            string insertCommand = string.Format("insert into Products (Name,Description,Category,Price) Values ('{0}','{1}','{2}',{3})", name, desc, cat, price);

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                conn.Open();

                SqlCommand command = new SqlCommand();
                command.CommandText = insertCommand;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                var result = command.ExecuteNonQuery();
                if (result > 0)
                {
                    Console.WriteLine("Product inserted successfully...");
                }
                else
                {
                    Console.WriteLine("Some problem with insertion...");
                }
                conn.Close();
            }
            Console.ReadLine();

            //using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            //{
            //    // open connection with database
            //    conn.Open();

            //    // create required command 
            //    SqlCommand command = new SqlCommand();
            //    Console.WriteLine("Enter Product Id:");
            //    int id = Convert.ToInt32(Console.ReadLine());
            //    command.CommandText = "select * from Products where Id=" + id;
            //    command.CommandType = System.Data.CommandType.Text;
            //    command.Connection = conn;

                

            //    // execute command to get data from database
            //    SqlDataReader reader = command.ExecuteReader();  

            //    // use data from data reader object
            //    while (reader.Read())
            //    {
            //        Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", reader["Id"], reader["Name"], reader["Description"],reader["Category"],reader["Price"]));
            //        Console.WriteLine();
            //    }
            //    reader.Close();
            //    conn.Close();

            //    Console.ReadLine();
            //}
        }
    }
}
