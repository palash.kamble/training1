﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDetails md = new MyDetails { FirstName = "John", LastName = "Boguslawski" };

            Printer.Print(md);

            Console.ReadLine();
        }
    }
}
