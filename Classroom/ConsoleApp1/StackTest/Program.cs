﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static List<Product> products;
        static int prdId = 0;

        static void Main(string[] args)
        {

            //Stack stack = new Stack();
            //bool aBoolean = true;
            //char aChar = '$';
            //int anInt = 1234;
            //string aString = "Hello";

            //// use Push()
            //stack.Push(aBoolean);
            //PrintStack(stack);
            //stack.Push(aChar);
            //PrintStack(stack);
            //stack.Push(anInt);
            //PrintStack(stack);
            //stack.Push(aString);
            //PrintStack(stack);

            //Console.WriteLine("First item in stack is {0}",stack.Peek());
            //PrintStack(stack);

            //object o = stack.Pop();
            //PrintStack(stack);

            //Console.ReadLine();

            products = new List<Product>();
            bool choice = true;
            Console.WriteLine("Enter 0 to end");
            while (choice)
            {
                Console.WriteLine("Enter Product Name:");
                string value = Console.ReadLine();
                if (value == "0")
                {
                    choice = false;
                    break;
                }
                else
                {
                    products.Add(new Product { ID = (prdId + 1), Name = value });
                    prdId += 1;
                }
            }

            Console.WriteLine(products);
            printList();
            
            Console.ReadLine();

        }

        private static void printList()
        {
            foreach (var item in products)
            {
                Console.WriteLine(item);
            }
        }

        private static void PrintStack(Stack stack)
        {
            if (stack.Count == 0)
            {
                Console.WriteLine("Stack is Empty\n");
            }
            else
            {
                Console.WriteLine("Stack is: ");
                foreach (var item in stack)
                {
                    Console.Write("{0} ", item);
                }

                Console.WriteLine();
            }
        }
    }
}
