﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsApp
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        private static int[] intValuesCopy;


        static void Main(string[] args)
        {
            //1. Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial array values:\n");
            PrintArrays();

            // sort doubleValues
            Array.Sort(doubleValues);

            // Copy values from intValues to intValuesCopy Array
            Array.Copy(intValues, intValuesCopy,intValues.Length);

            Console.WriteLine("\nArray values after sort and copy:\n");
            PrintArrays();

            // Search 5 intValues array
            int res = Array.BinarySearch(intValues,5);
            if (res >= 0)
            {
                Console.WriteLine("Element 5 found at posiiton {0}",res);
            }
            else
            {
                Console.WriteLine("5 not found in intValues");
            }

            // Search 8783 in intValues
            int res1 = Array.BinarySearch(intValues, 8783);
            if (res1 > 0)
            {
                Console.WriteLine("8783 found at element {0}",res1);
            }
            else
            {
                Console.WriteLine("8783 not found in intValues");
            }

            Console.ReadLine();
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues:");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValues: ");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValuesCopy: ");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element + " ");
            }
            Console.WriteLine();
        }

    }
}
