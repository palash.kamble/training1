﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTest
{
    class Program
    {
        private static readonly string[] colors = { "Magenta", "Red", "Blue","Cyan","White" };
        private static readonly string[] removeColors = { "Red", "White", "Blue" };

        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(1);
            // Add elements from static array into this list
            foreach (var color in colors)
            {
                list.Add(color);
            }

            ArrayList removeList = new ArrayList(removeColors);
            DisplayInfo(list);

            // remove colors
            removeColor(list,removeList);
            Console.WriteLine("\nArrayList after removing colors: ");
            DisplayInfo(list);

            Console.ReadLine();
        }

        private static void removeColor(ArrayList list, ArrayList removeList)
        {
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);
            }
        }

        private static void DisplayInfo(ArrayList list)
        {
            // iterate through list
            foreach (var item in list)
            {
                Console.Write("{0}",item + " ");
            }

            Console.WriteLine("\nSize: {0}; Capacity: {1}",list.Count,list.Capacity);
            list.Add("Black");

            int index = list.IndexOf("Blue");
            if (index != -1)  
            {
                Console.WriteLine("The array list contains blue at index: {0}",index);
            }
            else
            {
                Console.WriteLine("The array list does not contain blue anywhere");
            }
        }
    }
}
