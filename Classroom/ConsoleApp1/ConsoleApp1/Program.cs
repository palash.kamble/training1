﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    class Program
    {

        static void change(ref int x)
        {
            Console.WriteLine("In change()");
            Console.WriteLine("x before change is {0}", x);

            x *= 500;

            Console.WriteLine("x after change is {0}", x);

        }

        static void DrawPolygon(params int[] points)
        {
            Console.WriteLine("Polygon of {0} sides",points.Length);
        }

        static void Main(string[] args)
        {

            //int x = 50;
            //Console.WriteLine("In main()");
            //Console.WriteLine("x before calling  is {0}", x);

            //change(ref x);

            //Console.WriteLine("In main()");
            //Console.WriteLine("x after calling  is {0}", x);

            DrawPolygon(10, 12);
            DrawPolygon(10, 12, 13);
        }
    }



    
    public interface  IPrinter{
        void print();
        
    }

    public class laserprinter : IPrinter
    {
        public void print()
        {
            Console.WriteLine("Printing using laser......");
        }
    }

    public class DotMatrix : IPrinter
    {
        public void print()
        {
            Console.WriteLine("Printing using dotmatix......");
        }
    }




}
