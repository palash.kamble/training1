﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class MyArray
    {
        // Variables must be private
        private const int MAX_LENGTH = 100;
        private int length;

        public MyArray()
        {
           
        }

        public void NonStaticMethod()
        {

        }

        public static void StaticMethod()
        {

        }

    }

    public struct point
    {
        int x, y;
        public point(int a,int b)
        {
            x = a;
            y = b;
        }

        public int X { get { return x; }  set { x = value; } }

    }
}
