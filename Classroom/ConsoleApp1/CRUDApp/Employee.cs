﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDApp
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeDesignation { get; set; }
        public double EmployeeBasicSalary { get; set; }
        public double EmployeeDA { get; set; }
        public double EmployeeHRA { get; set; }
        public double EmployeePF { get; set; }
        public double EmployeeGrossSalary { get; set; }
        public double EmployeeNetSalary { get; set; }
        public int EmployeeDID { get; set; }

        public override string ToString()
        {
            return string.Format("Employee ID: " + EmployeeID 
                + "\nEmployee Name: " + EmployeeName
                + "\nEmployee Designation: " + EmployeeDesignation
                + "\nEmployee Basic Salary: " + EmployeeBasicSalary
                + "\nEmployee HRA: " + EmployeeHRA
                + "\nEmployee DA: " + EmployeeDA
                + "\nEmployee PF: " + EmployeePF
                + "\nEmployee Gross Salary: " + EmployeeGrossSalary
                + "\nEmployee Net Salary: " + EmployeeNetSalary
                + "\nDepartment ID: " + EmployeeDID);

        }

        public void calculateSal()
        {
            EmployeeHRA = (8 * EmployeeBasicSalary) / 100;
            EmployeePF = (12 * EmployeeBasicSalary) / 100;
            EmployeeGrossSalary = EmployeeBasicSalary + EmployeeHRA + 1250;
            EmployeeNetSalary = EmployeeBasicSalary + (10000 - EmployeePF);
        }

    }
}
