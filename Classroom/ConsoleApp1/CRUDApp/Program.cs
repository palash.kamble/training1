﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace CRUDApp
{
    class Program
    {
        static List<Department> departmentsList = new List<Department>();
        static List<Employee> employeeList = new List<Employee>();
        static HashSet<int> hsetDID = new HashSet<int>();
        static int autoID = 2001;


        static void Main(string[] args)
        {

            bool choice = true;
            while (choice)
            {
                Console.Clear();
                Console.WriteLine("Menu");
                Console.WriteLine("1. Employee Operations");
                Console.WriteLine("2. Department Operations");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Enter your choice:");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        bool chdd = true;
                        while (chdd)
                        {
                            
                            Console.WriteLine("\nEmployee Operations");
                            Console.WriteLine("1. Create Employee");
                            Console.WriteLine("2. Read all Employee details");
                            Console.WriteLine("3. Read specific Employee details");
                            Console.WriteLine("4. Update Employee details");
                            Console.WriteLine("5. Delete Employee");
                            Console.WriteLine("6. Go back");
                            Console.WriteLine("Enter your choice");
                            int chd_1 = Convert.ToInt32(Console.ReadLine());
                            switch (chd_1)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Enter Employee Name:");
                                    string empName = Console.ReadLine();
                                    Console.WriteLine("Enter Employee Designation:");
                                    string empDesignation = Console.ReadLine();
                                    Console.WriteLine("Enter Basic Salary:");
                                    double empBasicSalary = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("Enter DA: ");
                                    double empDA = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("Enter Department ID:");
                                    int empDID = Convert.ToInt32(Console.ReadLine());
                                    if (hsetDID.Contains(empDID))
                                    {
                                        Employee e = new Employee { EmployeeID = autoID++, EmployeeName = empName, EmployeeDesignation = empDesignation, EmployeeBasicSalary = empBasicSalary, EmployeeDA = empDA, EmployeeDID = empDID };
                                        e.calculateSal();
                                        employeeList.Add(e);
                                        foreach (var depart in departmentsList)
                                        {
                                            if (depart.DepartmentID == empDID)
                                            {
                                                depart.DEmployeeCount += 1;
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        Console.WriteLine("Department ID does not exists\nOperation failed");
                                    }
                                    

                                    break;
                                case 2:
                                    Console.Clear();
                                    Console.WriteLine("Displaying all employees");
                                    foreach (var allEmps in employeeList)
                                    {
                                        Console.WriteLine();
                                        Console.WriteLine(allEmps);
                                    }
                                    break;
                                case 3:
                                    Console.Clear();
                                    Console.WriteLine("Enter employee id to show its details:");
                                    int empid = Convert.ToInt32(Console.ReadLine());
                                    foreach (var emp in employeeList)
                                    {
                                        if (emp.EmployeeID == empid)
                                        {
                                            Console.WriteLine(emp);
                                        }
                                    }
                                    break;
                                case 4:
                                    Console.Clear();
                                    Console.WriteLine("Enter employee id to update its details");
                                    int empid1 = Convert.ToInt32(Console.ReadLine());
                                    foreach (var emp in employeeList)
                                    {
                                        if (emp.EmployeeID == empid1)
                                        {
                                            bool cch = true;
                                            while (cch)
                                            {
                                                Console.WriteLine("Press 1 to update name");
                                                Console.WriteLine("Press 2 to update designation");
                                                Console.WriteLine("Press 3 to update basic salary");
                                                Console.WriteLine("Press 4 to update DA");
                                                Console.WriteLine("Press 5 to go back");
                                                int echoice = Convert.ToInt32(Console.ReadLine());
                                                switch (echoice)
                                                {
                                                    case 1:
                                                        Console.WriteLine("Enter employee name to be updated");
                                                        string ename = Console.ReadLine();
                                                        emp.EmployeeName = ename;
                                                        break;
                                                    case 2:
                                                        Console.WriteLine("Enter employee designation to be updated");
                                                        string edesignation = Console.ReadLine();
                                                        emp.EmployeeDesignation = edesignation;
                                                        break;
                                                    case 3:
                                                        Console.WriteLine("Enter salary to be updated");
                                                        double esal = Convert.ToDouble(Console.ReadLine());
                                                        emp.EmployeeBasicSalary = esal;
                                                        break;
                                                    case 4:
                                                        Console.WriteLine("Enter DA to be updated");
                                                        double eDA = Convert.ToDouble(Console.ReadLine());
                                                        emp.EmployeeDA = eDA;
                                                        break;
                                                    default:
                                                        cch = false;
                                                        break;
                                                }
                                                Console.WriteLine("Updated Successfully!!");

                                                emp.calculateSal();
                                            }

                                            
                                        }
                                    }

                                    break;
                                case 5:
                                    Console.Clear();
                                    Console.WriteLine("Enter employee id to be deleted:");
                                    int employeeId = Convert.ToInt32(Console.ReadLine());
                                    int index = 0;
                                    foreach (var emp in employeeList)
                                    {
                                        if (emp.EmployeeID == employeeId)
                                        {
                                            employeeList.RemoveAt(index);
                                            break;
                                        }
                                        index++;
                                    }
                                    Console.WriteLine("Employee deleted successfully!!");
                                    break;
                                case 6:
                                    chdd = false;
                                    break;
                                default:
                                    break;
                            }

                        }
                        break;
                    case 2:
                        bool chd = true;
                        while (chd)
                        {
                            Console.WriteLine();
                            Console.WriteLine("Department Operations");
                            Console.WriteLine("1. Create Department");
                            Console.WriteLine("2. Read all Department details");
                            Console.WriteLine("3. Read specific Department details");
                            Console.WriteLine("4. Update Department details");
                            Console.WriteLine("5. Delete Department");
                            Console.WriteLine("6. Go back");
                            Console.WriteLine("Enter your choice");
                            int chd_1 = Convert.ToInt32(Console.ReadLine());
                            switch (chd_1)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Enter Department ID:");
                                    int deptId = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Enter Department Name:");
                                    string deptName = Console.ReadLine();
                                    Console.WriteLine("Enter Department Location:");
                                    string deptLocation = Console.ReadLine();

                                    departmentsList.Add(new Department { DepartmentID = deptId, DepartmentName = deptName, DepartmentLocation = deptLocation });
                                    hsetDID.Add(deptId);
                                    Console.WriteLine("Department added successfully!!");
                                    break;
                                case 2:
                                    Console.Clear();
                                    Console.WriteLine("Displaying all department details");
                                    foreach (var allDepts in departmentsList)
                                    {
                                        Console.WriteLine(allDepts);
                                    }
                                    break;
                                case 3:
                                    Console.Clear();
                                    Console.WriteLine("Enter department id to show its details:");
                                    int deptid1 = Convert.ToInt32(Console.ReadLine());
                                    foreach (var dept in departmentsList)
                                    {
                                        if (dept.DepartmentID == deptid1)
                                        {
                                            Console.WriteLine(dept);
                                        }
                                    }
                                    break;
                                case 4:
                                    Console.Clear();
                                    Console.WriteLine("Enter department id to update details");
                                    int deptid2 = Convert.ToInt32(Console.ReadLine());
                                    foreach (var dept in departmentsList)
                                    {
                                        if (dept.DepartmentID == deptid2)
                                        {
                                            Console.WriteLine("Enter department name to be updated");
                                            string dname = Console.ReadLine();
                                            Console.WriteLine("Enter departmnet location to be updated");
                                            string dlocation = Console.ReadLine();
                                            dept.DepartmentName = dname;
                                            dept.DepartmentLocation = dlocation;
                                        }
                                    }

                                    break;
                                case 5:
                                    Console.Clear();
                                    Department d = new Department();
                                    Console.WriteLine("Enter department id to be deleted:");
                                    int deleteId = Convert.ToInt32(Console.ReadLine());
                                    int index = 0;
                                    foreach (var dept in departmentsList)
                                    {
                                        if (dept.DepartmentID == deleteId )
                                        {
                                            if (dept.DEmployeeCount == 0)
                                            {
                                                departmentsList.RemoveAt(index);
                                                Console.WriteLine("Deleted successfully");
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Cannot be deleted.");
                                            }
                                            
                                        }
                                        index++;
                                    }
                                    break;
                                case 6:
                                    chd = false;
                                    break;
                                default:
                                    break;
                            }

                        }
                        break;

                    case 3:
                        choice = false;
                        Console.Clear();
                        Console.WriteLine("Thankyou");
                        break;
                    default:
                        break;
                }


            }


        }
    }
}
