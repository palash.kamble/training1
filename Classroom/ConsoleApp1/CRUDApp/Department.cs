﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDApp
{
    public class Department
    {

        private int DeptID;
        private string DeptName;
        private string DeptLocation;
        private int EmployeeCount=0;

        public int DepartmentID
        {
            get
            {
                return DeptID;
            }
            set
            {
                DeptID = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return DeptName;
            }
            set
            {
                DeptName = value;
            }
        }

        public string DepartmentLocation
        {
            get
            {
                return DeptLocation;
            }
            set
            {
                DeptLocation = value;
            }
        }

        public int DEmployeeCount { get { return EmployeeCount; } set { EmployeeCount = value; } }


        public override string ToString()
        {
            return string.Format("ID:{0}\tName:{1}\tLocation:{2}", DepartmentID, DepartmentName,DepartmentLocation);
        }

       
    }
}
