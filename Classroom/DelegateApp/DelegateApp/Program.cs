﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    delegate void Compute(int a);


    class Program
    {
        static void Main(string[] args)
        {

            // C# 2.0 way of writing
            //Compute c = new Compute(Square); 

            ////Compute c = Square;   C# 3.0 & later on
            //var res = c.Invoke(30);
            //Console.WriteLine("Square of 30 is " + res);

            //Compute c = Square;
            //c += Cube;
            //c.Invoke(12);

            Console.ReadLine();
        }

        static void Square(int x)
        {
            Console.WriteLine("Square of {0} is {1}",x,(x*x));
        }


        static void Cube(int y)
        {
            Console.WriteLine("Cube of {0} is {1}",y,(y*y*y));
        }
    }
}
