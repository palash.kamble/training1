﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace searchFlights
{
    class Program
    {
        static string origin;
        static string destination;
        
        static HashSet<string> hset = new HashSet<string>();
        static List<Flights> fList = new List<Flights>();

        static void Main(string[] args)
        {
            origin = args[1];
            destination = args[3];

            FileStream fs1 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider1.txt");
            FileStream fs2 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider2.txt");
            FileStream fs3 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider3.txt");
            StreamWriter writer = new StreamWriter(@"C:\training1\Classroom\searchFlights\final.txt", false);

            StreamReader reader1 = new StreamReader(fs1);
            StreamReader reader2 = new StreamReader(fs2);
            StreamReader reader3 = new StreamReader(fs3);

            string line;

            while ((line = reader1.ReadLine()) != null)
            {
                if (!hset.Contains(line))
                {
                    hset.Add(line);
                }
                else
                {
                    //Console.WriteLine(line);
                }
                
            }
            while ((line = reader2.ReadLine()) != null)
            {
                string[] data = line.Split(',');
                string finalLine = "";
                foreach (var idata in data)
                {
                    string line1 = idata;
                    string[] dateTime = line1.Split(' ');
                    string date;
                    if (dateTime.Length != 1)
                    {
                        //DateTime dt = DateTime.ParseExact(dateTime[0], "MM/dd/yyyy HH:mm:ss", null);
                        //Console.WriteLine(dt.ToString("MM/dd/yyyy"));
                        date = Flights.setDateFormat(dateTime[0]) + " " + dateTime[1];
                        finalLine += date + ",";
                    }
                    else
                    {
                        finalLine += idata + ",";
                    }

                }
                if (!hset.Contains(finalLine))
                {
                    hset.Add(finalLine);
                }
                else
                {
                    //Console.WriteLine(finalLine);
                }
                //hset.Add(finalLine.Substring(0, finalLine.Length - 1));
               
            }
            while ((line = reader3.ReadLine()) != null)
            {
                string[] data = line.Split('|');
                string finalLine = "";
                foreach (var idata in data)
                {
                    finalLine += idata + ",";
                }
                if (!hset.Contains(finalLine))
                {
                    hset.Add(finalLine);
                }
                else
                {
                    //Console.WriteLine(finalLine);
                }
                //hset.Add(finalLine.Substring(0, finalLine.Length - 1));
                
            }

            foreach (var str in hset)
            {
                writer.WriteLine(str);
            }

            writer.Close();

            PrintFlightDetails(origin,destination);
            //Console.WriteLine(hset.Count());

        }

        private static void PrintFlightDetails(string origin,string destination)
        {
            FileStream fsOpen = File.OpenRead(@"C:\training1\Classroom\searchFlights\final.txt");
            StreamReader reader = new StreamReader(fsOpen);
            string line;
            bool found = false;
            while ((line=reader.ReadLine()) != null)
            {
                string[] data = line.Split(',');
                if (data[0] == origin && data[2] == destination)
                {
                    found = true;
                    Console.WriteLine("{0} --> {1} ({2} --> {3}) - {4}",origin,destination,data[1],data[3],data[4]);
                }
            }
            if (!found)
            {
                Console.WriteLine("No Flights Found for {0} --> {1}",origin,destination);
            }
        }
    }
}
