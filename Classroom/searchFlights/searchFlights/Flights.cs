﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace searchFlights
{
    public class Flights
    {
        public string Origin { get; set; }
        public string DepartureTime{ get; set; }
        public string Destination { get; set; }
        public string DestinationTime { get; set; }
        public string Price { get; set; }

        public static string setDateFormat(string date)
        {
            string returnDate = "";
            string[] dateValues = date.Split('-');
            foreach (var d in dateValues)
            {
                returnDate += d + "/";
            }
            return returnDate.Substring(0, returnDate.Length - 1);

        }
    }
}
