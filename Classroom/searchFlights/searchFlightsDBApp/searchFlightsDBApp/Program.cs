﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace searchFlightsDBApp
{
    class Program
    {
        static string insertAirways;
        static List<string> jetList = new List<string>();
        static List<string> indigoList = new List<string>();
        static List<string> qatarList = new List<string>();
        static HashSet<string> hset;
        static string origin;
        static string destination;

        static void Main(string[] args)
        {
            origin = args[1];
            destination = args[3];

            FileStream fs1 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider1.txt");
            FileStream fs2 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider2.txt");
            FileStream fs3 = File.OpenRead(@"C:\training1\Classroom\searchFlights\Provider3.txt");
            StreamWriter writer = new StreamWriter(@"C:\training1\Classroom\searchFlights\final.txt", false);

            StreamReader reader1 = new StreamReader(fs1);
            StreamReader reader2 = new StreamReader(fs2);
            StreamReader reader3 = new StreamReader(fs3);

            string line;

            hset = new HashSet<string>();
            while ((line=reader1.ReadLine()) != null)
            {
                string[] data = line.Split(',');
                insertAirways = string.Format("insert into JetAirways Values ('{0}','{1}','{2}','{3}',{4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                hset.Add(insertAirways);
            }
            foreach (var item in hset)
            {
                jetList.Add(item);
            }

            hset = new HashSet<string>();
            while ((line = reader2.ReadLine()) != null)
            {
                string line1 = line.Replace('-', '/');
                string[] data = line1.Split(',');
                insertAirways = string.Format("insert into Indigo Values ('{0}','{1}','{2}','{3}',{4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                hset.Add(insertAirways);
            }
            foreach (var item in hset)
            {
                indigoList.Add(item);
            }

            hset = new HashSet<string>();
            while ((line = reader3.ReadLine()) != null)
            {
                string line1 = line.Replace('|', ',');
                string[] data = line1.Split(',');
                insertAirways = string.Format("insert into Qatar Values ('{0}','{1}','{2}','{3}',{4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                hset.Add(insertAirways);
            }
            foreach (var item in hset)
            {
                qatarList.Add(item);
            }

            jetList.Distinct().ToList();
            indigoList.Distinct().ToList();
            qatarList.Distinct().ToList();

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=flightsDB;Integrated Security=True;Pooling=False") )
            {
                conn.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = "delete from JetAirways";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();
                    
                for (int i = 0; i < jetList.Count; i++)
                {
                    //Console.WriteLine(jetList[i]);
                    command.CommandText = jetList[i];
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }

                //command.CommandText = "select * from JetAirways";
                //command.CommandType = System.Data.CommandType.Text;
                //command.Connection = conn;

                //SqlDataReader freader = command.ExecuteReader();
                //while (freader.Read())
                //{
                //    Console.WriteLine(string.Format("Origin: {0}\tDeparture Time: {1}\tDestination: {2}\tDestination Time: {3}\tPrice: {4}", freader["Origin"], freader["Departure Time"], freader["Destination"], freader["Destination Time"], freader["Price"]));
                //    Console.WriteLine();
                //}
                //freader.Close();


                command.CommandText = "delete from Indigo";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();

                for (int i = 0; i < indigoList.Count; i++)
                {
                    //Console.WriteLine(indigoList[i]);
                    command.CommandText = indigoList[i];
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }

                command.CommandText = "delete from Qatar";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();
                for (int i = 0;  i < qatarList.Count; i++)
                {
                    //Console.WriteLine(qatarList[i]);
                    command.CommandText = qatarList[i];
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }

                conn.Close();

                //Console.WriteLine("Flight details added successfully!!");

            }

            printFlights();

            Console.ReadLine();

        }

        private static void printFlights()
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=flightsDB;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "select * from JetAirways where Origin = '" + origin + "' AND Destination = '" + destination + "'" +
                    "UNION select * from Indigo where Origin = '" + origin + "' AND Destination = '" + destination + "'" +
                    "UNION select * from Qatar where Origin = '" + origin + "' AND Destination = '" + destination + "'";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                SqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    Console.WriteLine("No Flights Found for {0} --> {1}", origin, destination);
                }
                while (reader.Read())
                {
                    Console.WriteLine("{0} --> {1} ({2} --> {3}) - {4}", reader["Origin"], reader["Destination"], reader["Departure Time"], reader["Destination Time"], reader["Price"]);
                }
                reader.Close();
                conn.Close();
            }
        }
    }
}
