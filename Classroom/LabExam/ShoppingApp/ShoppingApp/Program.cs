﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Interfaces
{
    
    class Program
    {
        private static IListProducts prd;
        private static ICartInterface cart;
        private static int choice1;


        static void Main(string[] args)
        {
            bool ch1 = true;
            while (ch1)
            {
                Console.WriteLine("-----------Shopping Application-------------");
                Console.WriteLine("Press 1 to see list of all products");
                Console.WriteLine("Press 2 to see list of all products category wise");
                Console.WriteLine("Press 3 to add product to cart");
                Console.WriteLine("Press 4 to see cart details");
                Console.WriteLine("Press 5 to add/update quantity of product in your cart");
                Console.WriteLine("Press 6 to remove product from cart");
                Console.WriteLine("Press 7 to see cart summary");
                Console.WriteLine("Press 8 to place order");
                Console.WriteLine("Press 9 to exit");
                Outer:
                try
                {
                    // Menu 1
                    choice1 = Convert.ToInt32(Console.ReadLine());
                    switch (choice1)
                    {
                        case 1:
                            prd = new Models.AllProducts();
                            Console.WriteLine("\nFetching all products from database..........");
                            prd.PrintProducts();
                            break;
                        case 2:
                            Console.Clear();
                            prd = new Models.CategoryWiseProducts();
                            prd.PrintProducts();
                            break;
                        case 3:
                            Console.Clear();
                            Models.Products addPrd = new Models.Products();
                            addPrd.AddProductToCart();
                            break;
                        case 4:
                            Console.Clear();
                            cart = new Models.CartDetails();
                            cart.PrintCartDetails();
                            break;
                        case 5:
                            Console.Clear();
                            Models.CartClass cart_add = new Models.CartClass();
                            cart_add.AddQuantity();
                            break;
                        case 6:
                            Console.Clear();
                            Models.CartClass cart_remove = new Models.CartClass();
                            cart_remove.RemoveProduct();
                            break;
                        case 7:
                            Console.Clear();
                            cart = new Models.CartSummary();
                            cart.PrintCartDetails();
                            break;
                        case 8:
                            Console.Clear();
                            Models.CartClass cart_place = new Models.CartClass();
                            cart_place.PlaceOrder();
                            ch1 = false;
                            break;
                        case 9:
                            ch1 = false;
                            break;
                        default:
                            Console.WriteLine("Please enter correct option");
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Exception..........");
                    Console.WriteLine("Please enter valid input");
                    goto Outer;
                    
                }
            }

            Console.ReadLine();

        }

        

        

    }
}
