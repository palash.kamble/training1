﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class CartDetails : ICartInterface
    {

        public void PrintCartDetails()
        {
            var db = new ShoppingDBDataContext();

            var cartDetails = from cd in db.Carts
                              select cd;

            if (db.Carts.Count() != 0)
            {
                Console.WriteLine("Showing cart details");
                Console.WriteLine("---------------------------------------------------------------");
                Console.WriteLine("Id\tName\t\tQuantity\tPrice\t\tSubtotal");
                Console.WriteLine("---------------------------------------------------------------");
                foreach (var item in cartDetails)
                {
                    Console.WriteLine("{0}\t{1}\t{2}\t\t{3}\t\t{4}", item.Id, item.Name, item.Quantity, item.Price, item.Subtotal);
                }
                Console.WriteLine("\n");
                foreach (var item in cartDetails)
                {
                    //string output = String.Format("{0,-10}{1,-10}{2,-10}{3,-10}{4,-10}",item.Id,item.Name,item.Quantity,item.);
                    //Console.WriteLine(output);
                }
            }
            else
            {
                Console.WriteLine("Cart is empty!");
            }

        }
    }
}
