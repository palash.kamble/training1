﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class AllProducts : IListProducts
    {
        // Method for printing products
        public void PrintProducts()
        {
            // creating data context

            using (var db = new ShoppingDBDataContext())
            {
                // getting all table details
                var allProducts = from p in db.Products
                                  select p;
                foreach (var item in allProducts)
                {
                    Console.WriteLine("------------------");
                    Console.WriteLine("Id: {0}\nName: {1}\nDescription: {2}\nCategory: {3}\nPrice: {4}",
                         item.Id, item.Name, item.Description, item.Category, item.Price);
                    Console.WriteLine();
                }
            }


            
        }
    }
}
