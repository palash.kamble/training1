﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class Products
    {
        private int pro_id;

        public void AddProductToCart()
        {
            
            Console.WriteLine("Enter Product Id to be added to the cart");
            try
            {
                var db = new ShoppingDBDataContext();

                pro_id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter number of quantities to be added");
                int quantites = Convert.ToInt32(Console.ReadLine());

                var product = (from p in db.Products
                              where p.Id == pro_id
                              select p).SingleOrDefault();

                Cart c = new Cart();
                c.Name = product.Name;
                c.Quantity = quantites;
                c.Price = product.Price;
                c.Subtotal = quantites * product.Price;
                db.Carts.InsertOnSubmit(c);
                try
                {
                    db.SubmitChanges();
                    Console.WriteLine("Product added to the cart");
                }
                catch (Exception)
                {
                    Console.WriteLine("Error adding product to the cart");
                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Exception..........");
                Console.WriteLine("Please enter valid input");
                Console.WriteLine();
            }

        }
    }


}
