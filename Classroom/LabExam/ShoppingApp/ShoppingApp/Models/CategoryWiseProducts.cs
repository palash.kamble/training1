﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class CategoryWiseProducts : IListProducts
    {
        int choice;

        // To store all unique categories in hashset
        HashSet<String> hCategorySet = new HashSet<string>();

        // Inherited method
        public void PrintProducts()
        {
            
            var db = new ShoppingDBDataContext();

            var prd = from p in db.Products
                      select p;

            foreach (var item in prd)
            {
                hCategorySet.Add(item.Category);
            }

            // menu
            bool ch1 = true;
            while (ch1)
            {
                Console.WriteLine("Press 1 to enter category");
                Console.WriteLine("Press 2 to list all categories");
                Console.WriteLine("Press 3 to go back");
               
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.Write("Category: ");
                            string category = Console.ReadLine();

                            var catWisePrd = from cp in db.Products
                                             where cp.Category == category
                                             select cp;

                            Console.WriteLine("\nShowing products for category: {0}", category);
                            foreach (var item in catWisePrd)
                            {
                                Console.WriteLine("------------------");
                                Console.Write("Id: {0}\nName: {1}\n" +
                                    "Description: {2}\n" +
                                    "Price: {3}\n", item.Id, item.Name, item.Description, item.Price);
                                Console.WriteLine("------------------");
                                Console.WriteLine();
                            }

                            break;
                        case 2:
                            Console.WriteLine("List of categories-");
                            Console.WriteLine("------------------");
                            foreach (var item in hCategorySet)
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine("------------------");
                            break;
                        case 3:
                            Console.Clear();
                            ch1 = false;
                            break;
                        default:
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Exception.......");
                    Console.WriteLine("Please enter valid input");
                }
            }
        }
    }
}
