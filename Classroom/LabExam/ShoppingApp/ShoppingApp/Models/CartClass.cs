﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class CartClass
    {
        // Method to add quantity
        public void AddQuantity()
        {
            Console.WriteLine("Enter Id to add/update quantity in your cart:");
            int id = Convert.ToInt32(Console.ReadLine());

            // create data context
            var db = new ShoppingDBDataContext();
            var cartUpdate = from c in db.Carts
                             where c.Id == id
                             select c;

            if (cartUpdate.SingleOrDefault() != null)
            {
                Console.Write("Enter quantity: ");
                int quantity = Convert.ToInt32(Console.ReadLine());
                foreach (var item in cartUpdate)
                {
                    item.Quantity = quantity;
                    item.Subtotal = item.Price * quantity;
                }
                try
                {
                    db.SubmitChanges();
                    Console.WriteLine("Quantity added successfully");
                }
                catch (Exception)
                {
                    Console.WriteLine("Error adding quantity");
                }
            }
            else
            {
                Console.WriteLine("Id does not exist");
            }
            

        }

        // method for removing product
        public void RemoveProduct()
        {
            Console.WriteLine("Enter product id to be removed from cart");
            int pro_id = Convert.ToInt32(Console.ReadLine());

            var db = new ShoppingDBDataContext();

            var cartRemove = from rd in db.Carts
                       where rd.Id == pro_id
                       select rd;
            bool check = false;
            foreach (var item in cartRemove)
            {
                db.Carts.DeleteOnSubmit(item);
                db.SubmitChanges();
                check = true;
                
            }
            if (check)
            {
                Console.WriteLine("Product removed from cart successfully");
            }
            else
            {
                Console.WriteLine("Id does not exist");
            }
            
        }

        // method to place order
        public void PlaceOrder()
        {
            var db = new ShoppingDBDataContext();

            var cartPlace = from c in db.Carts
                            select c;

            db.Carts.DeleteAllOnSubmit(cartPlace);
            db.SubmitChanges();
            if (db.Carts.Count() == 0)
            {
                Console.WriteLine("Order Placed!!!!\nThank you for using our service.");
            }
            else
            {
                Console.WriteLine("Cart is empty");
            }
           
        }

    }
}
