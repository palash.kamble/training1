﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp.Models
{
    public class CartSummary : ICartInterface
    {

        public void PrintCartDetails()
        {
            var db = new ShoppingDBDataContext();

            var cart = from cs in db.Carts
                       select cs;

            int quantities = 0;
            double cart_price = 0;

            foreach (var item in cart)
            {
                quantities += item.Quantity;
                cart_price += (double)item.Subtotal;
            }

            Console.WriteLine("Cart Summary");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Total Cart Items\tTotal Price");
            Console.WriteLine("----------------------------------");
            Console.WriteLine(quantities + "\t\t\t$" + cart_price);
            Console.WriteLine("\n");
        }
    }
}
