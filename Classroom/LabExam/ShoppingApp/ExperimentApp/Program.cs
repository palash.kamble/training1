﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new ExperimentDBDataContext();

            Cart c = new Cart();

            var prod = (from p in db.Products
                        where p.Id == 11
                        select p).SingleOrDefault();

            // Add
            //c.Name = prod.Name;
            //c.Quantity = 10;
            //c.Price = prod.Price;
            //c.Subtotal = 10 * prod.Price;
            //db.Carts.InsertOnSubmit(c);

            // Update
            //var resc = from rc in db.Carts
            //           where rc.Id == 8
            //           select rc;
            //foreach (var item in resc)
            //{
            //    item.Quantity = 5;
            //    item.Subtotal = 5 * prod.Price;
            //}

            // remove
            var resd = from rd in db.Carts
                       where rd.Id == 8
                       select rd;

            foreach (var item in resd)
            {
                db.Carts.DeleteOnSubmit(item);
            }

            db.SubmitChanges();

        }
    }
}
