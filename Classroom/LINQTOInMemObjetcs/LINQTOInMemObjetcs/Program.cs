﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQTOInMemObjetcs
{
    class Program
    {
        static void Main(string[] args)
        {
            //NumQuery();
            //cityQuery();
            //XMLQuery();

            productsXMLQuery();


            Console.ReadLine();
        }

        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36 };

            var evenNumbers = from c in numbers
                              where (c%2 == 0)
                              select c;

            Console.WriteLine("Result: ");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        static IEnumerable<Customer> CreateCustomers()
        {

            // XML 
            return from c in XDocument.Load("Customers.XML").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value,
                       CustomerID = c.Attribute("CustomerID").Value };


            // ---------------

            //return new List<Customer>{
            //    new Customer{CustID = "IXC",City="Berlin"},
            //    new Customer{CustID = "DEL",City="Delhi"},
            //    new Customer{CustID = "MIA",City="Miami"},
            //    new Customer{CustID = "YYZ",City="YYZCity"},
            //    new Customer{CustID = "YYC",City="LONDON"},
            //    new Customer{CustID = "BOM",City="Bombay"},
            //    new Customer{CustID = "LON",City="LONDON"},
            //};



        }

        static void cityQuery()
        {
            var getCity = from c in CreateCustomers()
                          where (c.City == "London")
                          select c;

            foreach (var item in getCity)
            {
                Console.WriteLine(item);
            }
        }


        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.XML");

            var result = from c in doc.Descendants("Customer")
                         where c.Attribute("City").Value == "London"
                         select c;

            //Console.WriteLine("Result: ");
            //foreach (var item in result)
            //{
            //    Console.WriteLine("{0}\n",item);
            //}

            XElement transformDoc = new XElement("Londoners",
                                    from customer in result
                                    select new XElement("Contact",
                                    new XAttribute("ID",customer.Attribute("CustomerID").Value),
                                    new XAttribute("Name",customer.Attribute("ContactName").Value),
                                    new XAttribute("City",customer.Attribute("City").Value)));

            Console.WriteLine("Result:\n{0}",transformDoc);

            transformDoc.Save("Londoners.xml");
            Console.WriteLine("File saved...");

        }
        
        static void productsXMLQuery()
        {
            XElement productsDoc = new XElement("Products",
                                   new XElement("Product",
                                   new XAttribute("ID", "1"),
                                   new XElement("Name", "Foot Ball"),
                                   new XElement("Price","235")),
                                   new XElement("Product",
                                   new XAttribute("ID", "2"),
                                   new XElement("Name", "Racket"),
                                   new XElement("Price", "100")));

            Console.WriteLine("Reslut:\n{0}",productsDoc);
        }
    }
}
